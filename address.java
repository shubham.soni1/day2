package sheet1;

public class address {
	private int id;
	private int pin_code;
	private String city;
	private int student_id;

	address(int id, int pin_code, String city, int student_id) {
		this.id = id;
		this.pin_code = pin_code;
		this.city = city;
		this.student_id = student_id;
	}

	public String toString() {
		return "Id= " + id + ", Pin_Code= " + pin_code + " , City= " + city + " , Student_id= " + student_id;
	}

	public int id() {
		return id;
	}

	public int pin_code() {
		return pin_code;
	}

	public String city() {
		return city;
	}

	public int student_id() {
		return student_id;
	}
}

package sheet1;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class stream{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		classs c1 = new classs(1, "A");
		classs c2 = new classs(2, "B");
		classs c3 = new classs(3, "C");
		classs c4 = new classs(4, "D");

		ArrayList<classs> classs = new ArrayList<>();
		classs.add(c1);
		classs.add(c2);
		classs.add(c3);
		classs.add(c4);
		System.out.println(classs);

		
		ArrayList<address> addrs=new ArrayList<>();
      
      address add1 = new address(1, 452002, "indore", 1);
		address add2 = new address(2, 422002, "delhi", 1);
		address add3 = new address(3, 442002, "indore", 2);
		address add4 = new address(4, 462002, "delhi", 3);
		address add5 = new address(5, 472002, "indore", 4);
		address add6 = new address(6, 452002, "indore", 5);
		address add7 = new address(7, 452002, "delhi", 5);
		address add8 = new address(8, 482002, "mumbai ", 6);
		address add9 = new address(9, 482002, "bhopal", 7);
		address add10 = new address(10, 482002, "indore", 8);

		addrs.add(add1);
		addrs.add(add2);
		addrs.add(add3);
		addrs.add(add4);
		addrs.add(add5);
		addrs.add(add6);
		addrs.add(add7);
		addrs.add(add8);
		addrs.add(add9);
		addrs.add(add10);

		student students1 = new student(1, "stud1", "A", 88, "F", 10);
		student students2 = new student(2, "stud2", "A", 70, "F", 11);
		student students5 = new student(5, "stud5", "A", 30, "F", 44);
		student students6 = new student(6, "stud6", "C", 30, "F", 33);
		student students7 = new student(7, "stud7", "C", 10, "F", 22);
		student students8 = new student(8, "stud8", "C", 0, "M", 11);
        student students3 = new student(3, "stud3", "B", 86, "M", 22);
		student students4 = new student(4, "stud4", "B", 55, "M", 33);
	
		ArrayList<student> s1 = new ArrayList<>();

		s1.add(students1);
		s1.add(students2);
		s1.add(students3);
		s1.add(students4);
		s1.add(students5);
		s1.add(students6);
		s1.add(students7);
		s1.add(students8);

		
		Collections.sort(s1);
		s1.stream().forEach(e-> System.out.println(e));
		
		
//		filter(i-> i.id()==c.id()).
//		addrs.stream().filter(e-> e.pin_code()==452002).forEach(e->s1.stream().filter(y-> y.id()==e.id()).forEach(p->System.out.println(p)));
//	    addrs.stream().filter(c-> c.city().equals("indore")).forEach(c-> s1.stream().filter(s-> s.id()==c.student_id()).filter(y-> y.gender().equals("M")).forEach(print-> System.out.println(print)));
//	    s1.stream().filter(e-> e.age()>20).forEach(e-> System.out.println(e));
//	    s1.stream().filter(e-> e.gender().equals("F")).filter(e-> e.marks()>50).forEach(e-> classs.stream().filter(c-> c.name().equals("A")).forEach(c-> System.out.println(e)));
	    
//	    s1.stream().map(a-><3).forEach(a-> System.out.println());
	}

}

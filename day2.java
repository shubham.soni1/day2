package sheet1;

import java.util.*;

public class day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		classs c1 = new classs(1, "A");
		classs c2 = new classs(2, "B");
		classs c3 = new classs(3, "C");
		classs c4 = new classs(4, "D");

		ArrayList classs = new ArrayList();
		classs.add(c1);
		classs.add(c2);
		classs.add(c3);
		classs.add(c4);
		System.out.println(classs);

		student students1 = new student(1, "stud1", "A", 88, "F", 10);
		student students2 = new student(2, "stud2", "A", 70, "F", 11);
		student students3 = new student(3, "stud3", "B", 86, "M", 22);
		student students4 = new student(4, "stud4", "B", 55, "M", 33);
		student students5 = new student(5, "stud5", "A", 30, "F", 44);
		student students6 = new student(6, "stud6", "C", 30, "F", 33);
		student students7 = new student(7, "stud7", "C", 10, "F", 22);
		student students8 = new student(8, "stud8", "C", 0, "M", 11);

		ArrayList s1 = new ArrayList();

		s1.add(students1);
		s1.add(students2);
		s1.add(students3);
		s1.add(students4);
		s1.add(students5);
		s1.add(students6);
		s1.add(students7);
		s1.add(students8);

		System.out.println(s1);

		ArrayList addrs = new ArrayList();

		address add1 = new address(1, 452002, "indore", 1);
		address add2 = new address(2, 422002, "delhi", 1);
		address add3 = new address(3, 442002, "indore", 2);
		address add4 = new address(4, 462002, "delhi", 3);
		address add5 = new address(5, 472002, "indore", 4);
		address add6 = new address(6, 452002, "indore", 5);
		address add7 = new address(7, 452002, "delhi", 5);
		address add8 = new address(8, 482002, "mumbai ", 6);
		address add9 = new address(9, 482002, "bhopal", 7);
		address add10 = new address(10, 482002, "indore", 8);

		addrs.add(add1);
		addrs.add(add2);
		addrs.add(add3);
		addrs.add(add4);
		addrs.add(add5);
		addrs.add(add6);
		addrs.add(add7);
		addrs.add(add8);
		addrs.add(add9);
		addrs.add(add10);

		System.out.println(addrs);

		System.out.println("Enter 1 to filter Students by Pincode");
		System.out.println("Enter 2 to filter Students by Name");
		System.out.println("Enter 3 to filter Students by Age");
		System.out.println("Enter 4 to filter Students by City");
		System.out.println("Enter 5 to filter Students by Marks");
		System.out.println("Enter 6 to filter Students by Pass ");
		System.out.println("Enter 7 to filter Students by Fail");
		System.out.println("Enter 8 to filter Students by class");
		System.out.println("Enter 9 to Delete Student");
		System.out.println("Enter 10 to check class");
		System.out.println("Enter 11 to get Ordered by Female");
		System.out.println("Enter 12 to get Female by name");

		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		switch (choice) {
		case 1: {
			day2.pincode(addrs, s1, classs, "pin_code");
			break;
		}
		case 2: {
			day2.pincode(addrs, s1, classs, "name");
			break;
		}
		case 3: {
			day2.pincode(addrs, s1, classs, "age");
			break;
		}
		case 4: {
			day2.pincode(addrs, s1, classs, "city");
			break;
		}
		case 5: {
			day2.pincode(addrs, s1, classs, "marks");
			break;
		}
		case 6: {
			day2.pincode(addrs, s1, classs, "pass");
			break;
		}
		case 7: {
			day2.pincode(addrs, s1, classs, "fail");
			break;
		}
		case 8: {
			day2.pincode(addrs, s1, classs, "cls");
			break;
		}
		case 9: {
			day2.pincode(addrs, s1, classs, "delete");
			break;
		}
		case 10: {
			day2.pincode(addrs, s1, classs, "empty");
			break;
		}
		case 11: {
			day2.pincode(addrs, s1, classs, "gen");
			break;
		}
		case 12: {
			day2.pincode(addrs, s1, classs, "order_by_name");
			break;
		}
		}
//		day2.pincode(addrs, s1, "name");

	}

	static void pincode(List addrs, List s1, List cls, String value) {
		ArrayList std = new ArrayList();
		switch (value) {
	
		// To getting values by PinCode
		case "pin_code": {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter pin");
			int pin = sc.nextInt();
			for (int i = 0; i < addrs.size(); i++) {
				address ad = (address) addrs.get(i);
				if (ad.pin_code() == pin) {
					int x = ad.student_id();
					for (int j = 0; j < s1.size(); j++) {
						student st = (student) s1.get(j);
						if (st.id() == x) {
							std.add(st);
							break;
						}
					}
				}
			}
			System.out.println(std);
			break;
		}
		
		// TO getting values by Name
		case "name": {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Name");
			String name = sc.nextLine();

			for (int i = 0; i < s1.size(); i++) {
				student st = (student) s1.get(i);
				if (st.name().equals(name)) {
					int x = st.id();
					std.add(st);
					break;
				}
			}
			System.out.println(std);
			break;
		}
		
		// To getting values by Age
		case "age": {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Age");
			int age = sc.nextInt();
			for (int i = 0; i < s1.size(); i++) {
				student st = (student) s1.get(i);
				if (st.age() == age) {
					std.add(st);
				}
			}
			System.out.println(std);
			break;
		}
		
		//To getting values by City
		
		case "city": {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter City");
			String city = sc.nextLine();
			for (int i = 0; i < addrs.size(); i++) {
				address ad = (address) addrs.get(i);
				if (ad.city().equals(city)) {
					int x = ad.student_id();
					for (int j = 0; j < s1.size(); j++) {
						student st = (student) s1.get(j);
						if (st.id() == x) {
							std.add(st);
						}
						System.out.println(std);
						break;
					}
				}
			}
		}
		
		// To getting Values By Marks
		case "marks": {

			int first = 0, second = 0, third = 0;

			Collections.sort(s1);
			student sr = (student) s1.get(0);
			first = sr.marks();
			System.out.println(sr.name() + " = " + first);
			student sr1 = (student) s1.get(1);
			second = sr1.marks();
			System.out.println(sr1.name() + " = " + second);
			student sr2 = (student) s1.get(2);
			third = sr2.marks();
			System.out.println(sr2.name() + " = " + third);
			for (int i = 3; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				System.out.println(sr3.name() + "=" + sr3.marks());
			}

		}
		
		
        // TO getting values by Pass
		case "pass": {
			for (int i = 0; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				if (sr3.marks() < 50) {
				} else {
					System.out.println(sr3.name() + "=" + sr3.marks());
				}
			}
			break;
		}
		
		
        // TO getting values by Fail
		case "fail": {
			for (int i = 0; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				if (sr3.marks() > 50) {
				} else {
					System.out.println(sr3.name() + "=" + sr3.marks());
				}
			}
			break;
		}
      
		
//		to getting values by Class
		case "cls": {
			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter Class");
			String c1 = sc1.nextLine();

			for (int i = 0; i < s1.size(); i++) {
				student sc = (student) s1.get(i);
				if ((sc.class_id()).equals(c1)) {
					std.add(sc);
				}
			}
			System.out.println(std);
			break;
		   }
		

		//To Delete Values
		case "delete": {

			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter Student Id which you want to delete");
			int id=sc1.nextInt();

			for (int i = 0; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				if (sr3.id() == id) {
					s1.remove(i);
				} 
			}
			System.out.println(s1);
			break;
		}

		//to getting values by Gender
		case "gen": {
			
			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter Gender");
			String  gen=sc1.nextLine();
			int x=sc1.nextInt();
			int y=sc1.nextInt();
			for (int i = 0; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				if (sr3.gender().equals(gen)) {
					std.add(sr3);
				} 
			}
			for(int i=x;i<=y;i++)
			{
			System.out.println(std.get(i));
			}
			break;
		}
		
		//To getting values by Order Of name
		case "order_by_name": {
			
			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter Range");
			int x=sc1.nextInt();
			int y=sc1.nextInt();
			for (int i = 0; i < s1.size(); i++) {
				student sr3 = (student) s1.get(i);
				if (sr3.gender().equals("F")) {
					std.add(sr3);
				} 
			}
			for(int i=x;i<=y;i++)
			{
				System.out.println(std.get(i));
			}
			break;
		}
		
		//Delete that class this in which no students are present
		case "empty": {

			Scanner sc1 = new Scanner(System.in);
			System.out.println("Enter Student Class which you want to check");
			String id=sc1.nextLine();

			boolean cl=false;
			for(int i=0;i<s1.size();i++)
			{
				student st=(student)s1.get(i);
			if(st.class_id().equals(id))
			{
				cl=true;
			}
			}
			if(cl) {System.out.println("Students are present in class "+id);
			}
			else {
				for(int j=0;j<cls.size();j++)
				{
				classs c=(classs)cls.get(j);
				if(c.name().equals(id))
					{cls.remove(j);}
			
				}
			}
			System.out.println(cls);
			break;
		}

		
		
}
}
}

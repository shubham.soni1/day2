package sheet1;

import java.io.*;
import java.util.*;
import java.util.ArrayList;

public class q7 {
	public static void main(String arssd[]) throws Exception {
		String class1 = "E:\\java\\sheet1\\classs.csv";
       q7.clas(class1);           		
       String class2 = "E:\\java\\sheet1\\address1.csv";
       q7.address(class2);    
   	   String class3 = "E:\\java\\sheet1\\student1.csv";
   	   q7.student(class3);    
     
   	}
	
	static void clas(String classs)throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(classs));

		ArrayList cls = new ArrayList();

		String s1 = br.readLine();
		while (s1 != null) {
			String s2[] = s1.split(",");
			int id = Integer.parseInt(s2[0].trim());
			String name = s2[1];
			cls.add(new classs(id, name));
			s1 = br.readLine();
        }
		System.out.println(cls);
		br.close();
	}
	static void address(String classs)throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(classs));
		
		ArrayList adrr = new ArrayList();
		
		String s1 = br.readLine();
		while (s1 != null) {
			String s2[] = s1.split(",");
			int id = Integer.parseInt(s2[0].trim());
			int pin_code = Integer.parseInt(s2[1].trim());
			String city = s2[2];
			int student_id = Integer.parseInt(s2[3].trim());
			adrr.add(new address(id,pin_code,city,student_id));
			s1 = br.readLine();
		}
		System.out.println(adrr);
		br.close();
	}
	static void student(String classs)throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader(classs));
		
		ArrayList student = new ArrayList();
		
		String s1 = br.readLine();
		while (s1 != null) {
			String s2[] = s1.split(",");
			int id = Integer.parseInt(s2[0].trim());
			String name = s2[1];
			String class_id= s2[2];
			int marks = Integer.parseInt(s2[3].trim());
			String gender = s2[4];
			int age= Integer.parseInt(s2[5].trim());
			student.add(new student(id,name,class_id,marks,gender,age));
			s1 = br.readLine();
		}
		System.out.println(student);
		br.close();
	}

}

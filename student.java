
package sheet1;

public class student implements Comparable {
	private int id;
	private String name;
	private String class_id;
	private int marks;
	private String gender;
	private int age;

	student(int id, String name, String class_id, int marks, String gender, int age) {
		this.id = id;
		this.name = name;
		this.class_id = class_id;
		this.marks = marks;
		this.gender = gender;
		this.age = age;
	}

	public String toString() {
		return "id= " + id + " Marks= " + marks ;
	}

	public int id() {
		return id;
	}

	public String name() {
		return name;
	}

	public String class_id() {
		return class_id;
	}

	public int marks() {
		return marks;
	}

	public String gender() {
		return gender;
	}

	public int age() {
		return age;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
			student sr=(student)o;
//		return sr.marks()-this.marks();
	return sr.name().compareTo(this.name());
	}
	
	

}
